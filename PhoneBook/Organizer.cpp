#include "stdafx.h"
#include "Organizer.h"


Organizer::Organizer(std::vector<Person*> persons) :
	persons(persons)
{
}


Organizer::~Organizer()
{
}


void Organizer::addPerson(Person* person)
{
	persons.push_back(person);
}


Person* Organizer::getPerson(std::string name)
{
	Person* result = nullptr;
	std::vector<Person*>::iterator iterator;
	for (iterator = persons.begin(); iterator != persons.end(); iterator++)
	{
		if ((*iterator)->getFirstName() == name) 
		{
			result = (*iterator);
		}
	}
	return result;
}


std::vector<Person*> Organizer::getPersonsList()
{
	return persons;
}


void Organizer::removePerson(std::string name)
{
	std::vector<Person*>::iterator iterator;
	for (iterator = persons.begin(); iterator != persons.end(); )
	{
		if ((*iterator)->getFirstName() == name)
		{
			iterator = persons.erase(iterator);
		}
		else
		{
			iterator++;
		}
	}
}


std::string Organizer::toString()
{
	std::string result = "";
	std::vector<Person*>::iterator iterator;
	for (iterator = persons.begin(); iterator != persons.end(); iterator++)
	{
		result += (*iterator)->toString() + "\n";
	}
	return result;
}

std::vector<Person*> Organizer::searchBySubstring(std::string substring)
{
	std::vector<Person*> result;
	std::vector<Person*>::iterator iterator;
	for (iterator = persons.begin(); iterator != persons.end(); iterator++)
	{
		if ((*iterator)->getFirstName().find(substring) != std::string::npos)
		{
			result.push_back(*iterator);
		}
	}
	return result;
}
