#include "Person.h"
#include <vector>

#pragma once
class Organizer
{
public:
	Organizer(std::vector<Person*> persons);
	~Organizer();
	void addPerson(Person* person);
	Person* getPerson(std::string name);
	std::vector<Person*> getPersonsList();
	void removePerson(std::string name);
	std::string toString();
	std::vector<Person*> searchBySubstring(std::string substring);

private:
	std::vector<Person*> persons;
};

