#include "stdafx.h"
#include "Person.h"


Person::Person(std::string firstName, std::string lastName, std::string phoneNumber, std::string email) :
	firstName(firstName), lastName(lastName), phoneNumber(phoneNumber), email(email)
{
}


Person::~Person()
{
}


std::string Person::getFirstName()
{
	return Person::firstName;
}


std::string Person::getPhoneNumber()
{
	return Person::phoneNumber;
}


std::string Person::getEmail()
{
	return Person::email;
}


std::string Person::toString()
{
	return Person::firstName + " " + Person::lastName + " " + Person::phoneNumber + " " + Person::email;
}