#include <string>
#pragma once
class Person
{
public:
	Person(std::string firstName, std::string lastName, std::string phoneNumber, std::string email);
	~Person();
	std::string getFirstName();
	std::string getLastName();
	std::string getPhoneNumber();
	std::string getEmail();
	std::string toString();

private:
	std::string firstName;
	std::string lastName;
	std::string phoneNumber;
	std::string email;
};

