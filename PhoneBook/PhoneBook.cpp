#include "stdafx.h"
#include <iostream>
#include <vector>
#include "Person.h"
#include "Organizer.h"

using namespace std;

Person* findPersonByPhoneNumber(vector<Person*> phoneBook, string phoneNumber) 
{
	Person* result = nullptr;
	vector<Person*>::iterator iterator;
	for (iterator = phoneBook.begin(); iterator != phoneBook.end(); iterator++)
	{
		if (phoneNumber == (*iterator)->getPhoneNumber())
		{
			result = (*iterator);
		}
	}
	return result;
}

int main()
{
	vector<Person*> persons{
		new Person("Roman", "Rudyi", "0967428440", "roman.rudyi@gmail.com"),
		new Person("Volodymyr", "Romaniv", "0932233456", "volodymyr.romaniv@gmail.com"),
		new Person("Andriana", "Solyna", "0968998098", "andriana.solyna@gmail.com"),
		new Person("Mykhailo", "Berikashvili", "0674530377", "mykhailo.berukashvili@gmail.com"),
		new Person("Kateryna", "Balyk", "0987654113", "kateryna.balyk@gmail.com")
	};
	
	Organizer* organizer = new Organizer(persons);
	
	// get list of persons
	cout << organizer->toString() << "\n";

	cout << "Enter the name to search:\n";
	string name;
	cin >> name;

	Person* searchResult = organizer->getPerson(name);

	if (searchResult == nullptr)
	{
		cout << "There is no person with that name\n";
	}
	else
	{
		cout << searchResult->toString() << "\n";
	}

	cout << "Enter the name to remove:\n";
	cin >> name;
	organizer->removePerson(name);
	// get list of persons
	cout << organizer->toString() << "\n";

	cout << "Added new Person\n";
	// add new person
	organizer->addPerson(new Person("Roman", "Rudyi", "0967428440", "roman.rudyi@gmail.com"));
	// get list of persons
	cout << organizer->toString() << "\n";

	// Additional task C
	// Search by substrind
	std::string test = "an";
	vector<Person*> result = organizer->searchBySubstring(test);
	if (!result.empty())
	{
		Organizer* temp = new Organizer(result);
		cout << temp->toString() << "\n";
	}

	system("pause");
	return 0;
}

